EESchema Schematic File Version 4
LIBS:usb_m8
EELAYER 26 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 1 1
Title "USB M8"
Date "2018-05-26"
Rev "2"
Comp "Mx. Shift"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L usb_m8:FT232RL-REEL U101
U 1 1 5A9475C2
P 4800 2800
F 0 "U101" H 4300 3250 60  0000 C CNN
F 1 "FT232RL-REEL" V 4700 2400 60  0000 C CNN
F 2 "digikey-footprints:SSOP-28_W5.30mm" H 5000 3000 60  0001 L CNN
F 3 "http://www.ftdichip.com/Support/Documents/DataSheets/ICs/DS_FT232R.pdf" H 5000 3100 60  0001 L CNN
F 4 "768-1007-1-ND" H 5000 3200 60  0001 L CNN "Digi-Key_PN"
F 5 "FT232RL-REEL" H 5000 3300 60  0001 L CNN "MPN"
F 6 "Integrated Circuits (ICs)" H 5000 3400 60  0001 L CNN "Category"
F 7 "Interface - Controllers" H 5000 3500 60  0001 L CNN "Family"
F 8 "http://www.ftdichip.com/Support/Documents/DataSheets/ICs/DS_FT232R.pdf" H 5000 3600 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/ftdi-future-technology-devices-international-ltd/FT232RL-REEL/768-1007-1-ND/1836402" H 5000 3700 60  0001 L CNN "DK_Detail_Page"
F 10 "IC USB FS SERIAL UART 28-SSOP" H 5000 3800 60  0001 L CNN "Description"
F 11 "FTDI, Future Technology Devices International Ltd" H 5000 3900 60  0001 L CNN "Manufacturer"
F 12 "Active" H 5000 4000 60  0001 L CNN "Status"
	1    4800 2800
	1    0    0    -1  
$EndComp
NoConn ~ 3800 3400
NoConn ~ 3800 3500
NoConn ~ 3800 3600
NoConn ~ 3800 3700
NoConn ~ 3800 3800
NoConn ~ 5500 2900
NoConn ~ 5500 2800
NoConn ~ 3800 3000
NoConn ~ 3800 3100
NoConn ~ 3800 3200
NoConn ~ 3800 3300
NoConn ~ 5500 2600
NoConn ~ 3800 2800
Text Label 5600 2700 0    60   ~ 0
FTDI_DB9_TXD
Text Label 3700 2900 2    60   ~ 0
DB9_FTDI_RXD
Text Label 3700 2600 2    60   ~ 0
USB_FTDI_DM
Text Label 3700 2500 2    60   ~ 0
USB_FTDI_DP
$Comp
L usb_m8:GND #PWR106
U 1 1 5A9479FF
P 4800 4300
F 0 "#PWR106" H 4800 4050 50  0001 C CNN
F 1 "GND" H 4800 4150 50  0000 C CNN
F 2 "" H 4800 4300 50  0001 C CNN
F 3 "" H 4800 4300 50  0001 C CNN
	1    4800 4300
	1    0    0    -1  
$EndComp
$Comp
L usb_m8:+3.3V #PWR107
U 1 1 5A947ADC
P 5500 2350
F 0 "#PWR107" H 5500 2200 50  0001 C CNN
F 1 "+3.3V" H 5500 2490 50  0000 C CNN
F 2 "" H 5500 2350 50  0001 C CNN
F 3 "" H 5500 2350 50  0001 C CNN
	1    5500 2350
	1    0    0    -1  
$EndComp
$Comp
L usb_m8:VBUS #PWR105
U 1 1 5A947BC8
P 4650 2000
F 0 "#PWR105" H 4650 1850 50  0001 C CNN
F 1 "VBUS" H 4650 2150 50  0000 C CNN
F 2 "" H 4650 2000 50  0001 C CNN
F 3 "" H 4650 2000 50  0001 C CNN
	1    4650 2000
	1    0    0    -1  
$EndComp
Text Label 3700 2700 2    60   ~ 0
VBUS
$Comp
L usb_m8:CONN-4 J101
U 1 1 5A947D6B
P 2450 2550
F 0 "J101" H 2300 2800 60  0000 C CNN
F 1 "CONN-4" H 2350 2300 60  0001 C CNN
F 2 "" H 2500 2600 60  0001 C CNN
F 3 "" H 2500 2600 60  0001 C CNN
	1    2450 2550
	1    0    0    -1  
$EndComp
$Comp
L usb_m8:GND #PWR102
U 1 1 5A947E0F
P 2650 2800
F 0 "#PWR102" H 2650 2550 50  0001 C CNN
F 1 "GND" H 2650 2650 50  0000 C CNN
F 2 "" H 2650 2800 50  0001 C CNN
F 3 "" H 2650 2800 50  0001 C CNN
	1    2650 2800
	1    0    0    -1  
$EndComp
$Comp
L usb_m8:VBUS #PWR101
U 1 1 5A947E51
P 2650 2300
F 0 "#PWR101" H 2650 2150 50  0001 C CNN
F 1 "VBUS" H 2650 2450 50  0000 C CNN
F 2 "" H 2650 2300 50  0001 C CNN
F 3 "" H 2650 2300 50  0001 C CNN
	1    2650 2300
	1    0    0    -1  
$EndComp
$Comp
L usb_m8:CONN-10 J102
U 1 1 5A9480F7
P 7600 2900
F 0 "J102" H 7300 3450 60  0000 C CNN
F 1 "CONN-10" H 7500 2350 60  0001 C CNN
F 2 "" H 7600 3050 60  0001 C CNN
F 3 "" H 7600 3050 60  0001 C CNN
	1    7600 2900
	-1   0    0    -1  
$EndComp
$Comp
L usb_m8:GND #PWR110
U 1 1 5A9482CC
P 7350 3500
F 0 "#PWR110" H 7350 3250 50  0001 C CNN
F 1 "GND" H 7350 3350 50  0000 C CNN
F 2 "" H 7350 3500 50  0001 C CNN
F 3 "" H 7350 3500 50  0001 C CNN
	1    7350 3500
	1    0    0    -1  
$EndComp
Text Label 7250 2850 2    60   ~ 0
FTDI_DB9_TXD
Text Label 7250 3250 2    60   ~ 0
DB9_FTDI_RXD
$Comp
L usb_m8:C C101
U 1 1 5A948517
P 3000 5150
F 0 "C101" H 3025 5250 50  0000 L CNN
F 1 "10uF" H 3025 5050 50  0000 L CNN
F 2 "digikey-footprints:0603" H 3038 5000 50  0001 C CNN
F 3 "" H 3000 5150 50  0001 C CNN
	1    3000 5150
	1    0    0    -1  
$EndComp
$Comp
L usb_m8:C C102
U 1 1 5A948581
P 3300 5150
F 0 "C102" H 3325 5250 50  0000 L CNN
F 1 "100nF" H 3325 5050 50  0000 L CNN
F 2 "digikey-footprints:0603" H 3338 5000 50  0001 C CNN
F 3 "" H 3300 5150 50  0001 C CNN
	1    3300 5150
	1    0    0    -1  
$EndComp
$Comp
L usb_m8:GND #PWR104
U 1 1 5A948656
P 3600 5400
F 0 "#PWR104" H 3600 5150 50  0001 C CNN
F 1 "GND" H 3600 5250 50  0000 C CNN
F 2 "" H 3600 5400 50  0001 C CNN
F 3 "" H 3600 5400 50  0001 C CNN
	1    3600 5400
	1    0    0    -1  
$EndComp
$Comp
L usb_m8:VBUS #PWR103
U 1 1 5A94867E
P 2800 4900
F 0 "#PWR103" H 2800 4750 50  0001 C CNN
F 1 "VBUS" H 2800 5050 50  0000 C CNN
F 2 "" H 2800 4900 50  0001 C CNN
F 3 "" H 2800 4900 50  0001 C CNN
	1    2800 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 2800 4000 2800
Wire Wire Line
	3800 3000 4000 3000
Wire Wire Line
	3800 3100 4000 3100
Wire Wire Line
	4000 3200 3800 3200
Wire Wire Line
	4000 3300 3800 3300
Wire Wire Line
	4000 3400 3800 3400
Wire Wire Line
	4000 3500 3800 3500
Wire Wire Line
	4000 3600 3800 3600
Wire Wire Line
	4000 3700 3800 3700
Wire Wire Line
	4000 3800 3800 3800
Wire Wire Line
	5300 2600 5500 2600
Wire Wire Line
	5300 2800 5500 2800
Wire Wire Line
	5300 2900 5500 2900
Wire Wire Line
	4000 2900 3700 2900
Wire Wire Line
	5300 2700 5600 2700
Wire Wire Line
	2550 2500 4000 2500
Wire Wire Line
	2550 2600 4000 2600
Wire Wire Line
	4800 4100 4800 4200
Wire Wire Line
	4800 4200 4800 4300
Wire Wire Line
	4700 4100 4700 4200
Wire Wire Line
	4600 4200 4700 4200
Wire Wire Line
	4700 4200 4800 4200
Wire Wire Line
	4800 4200 4900 4200
Wire Wire Line
	4900 4200 5000 4200
Connection ~ 4800 4200
Wire Wire Line
	4600 4100 4600 4200
Connection ~ 4700 4200
Wire Wire Line
	4900 4200 4900 4100
Wire Wire Line
	5000 4200 5000 4100
Connection ~ 4900 4200
Wire Wire Line
	5300 2500 5500 2500
Wire Wire Line
	5500 2500 5500 2350
Wire Wire Line
	4600 2200 4600 2100
Wire Wire Line
	4600 2100 4650 2100
Wire Wire Line
	4650 2100 4700 2100
Wire Wire Line
	4650 2100 4650 2000
Wire Wire Line
	4700 2100 4700 2200
Connection ~ 4650 2100
Wire Wire Line
	4000 2700 3700 2700
Wire Wire Line
	2550 2700 2650 2700
Wire Wire Line
	2650 2700 2650 2800
Wire Wire Line
	2550 2400 2650 2400
Wire Wire Line
	2650 2400 2650 2250
Wire Wire Line
	7500 3350 7350 3350
Wire Wire Line
	7350 2950 7350 3350
Wire Wire Line
	7350 3350 7350 3500
Wire Wire Line
	7500 2950 7350 2950
Connection ~ 7350 3350
Wire Wire Line
	7500 2850 7250 2850
Wire Wire Line
	7500 3250 7250 3250
Wire Wire Line
	2800 4900 2800 4950
Wire Wire Line
	2800 4950 3000 4950
Wire Wire Line
	3000 4950 3300 4950
Wire Wire Line
	3000 4950 3000 5000
Wire Wire Line
	3300 4950 3300 5000
Connection ~ 3000 4950
Wire Wire Line
	3000 5300 3000 5350
Wire Wire Line
	3000 5350 3300 5350
Wire Wire Line
	3300 5350 3600 5350
Wire Wire Line
	3600 5350 3600 5400
Wire Wire Line
	3300 5300 3300 5350
Connection ~ 3300 5350
$Comp
L usb_m8:C C103
U 1 1 5A9489C3
P 6000 5150
F 0 "C103" H 6025 5250 50  0000 L CNN
F 1 "100nF" H 6025 5050 50  0000 L CNN
F 2 "digikey-footprints:0603" H 6038 5000 50  0001 C CNN
F 3 "" H 6000 5150 50  0001 C CNN
	1    6000 5150
	1    0    0    -1  
$EndComp
$Comp
L usb_m8:+3.3V #PWR108
U 1 1 5A948A01
P 6000 4900
F 0 "#PWR108" H 6000 4750 50  0001 C CNN
F 1 "+3.3V" H 6000 5040 50  0000 C CNN
F 2 "" H 6000 4900 50  0001 C CNN
F 3 "" H 6000 4900 50  0001 C CNN
	1    6000 4900
	1    0    0    -1  
$EndComp
$Comp
L usb_m8:GND #PWR109
U 1 1 5A948A2D
P 6000 5400
F 0 "#PWR109" H 6000 5150 50  0001 C CNN
F 1 "GND" H 6000 5250 50  0000 C CNN
F 2 "" H 6000 5400 50  0001 C CNN
F 3 "" H 6000 5400 50  0001 C CNN
	1    6000 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 4900 6000 5000
Wire Wire Line
	6000 5300 6000 5400
$EndSCHEMATC
