# Mx. Shift USB-M8

USB-M8 (pronounced USB mate) is a USB adapter compatible with interfaces designed for [MoTeC PCI cables](https://www.motec.com.au/ac-cc-seriallogiclevel/cc-seriallogic-pc/).

## Does this really work?

Yes. I use one regularly with a MoTeC M48.

## Where can I get one?

Finished cables will be sold via Tindie. If you prefer to build your own,
schematics are in this repository. A board layout will be added soon.

## Wait. So, I can build my own?

Yup. While I offer finished cables, anyone is free to use the design files here
to produce their own PCBs, full cables, or even improved designs. The only
requirement is attribution per [Creative Commons Attribution 4.0 International
License](https://creativecommons.org/licenses/by/4.0/).

## If anyone can make them, why are you selling them?

I originally produced this cable as I needed one for my race car. When I put up
my [blog post](https://www.kc8apf.net/2017/01/motec-pci-cable-for-20/)
documenting the PCI connector pinout, I started to hear that there was a desire
to buy finished cables.
